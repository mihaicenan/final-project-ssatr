package school.management.system;

/**
 * This class is responsible for keeping track of the students -id, name, grade, fees paid, fees total
 * Student id and name are not going to be changed -> no setter method
 * Fees for every student is 30.000/year
 */

public class Student extends Person{

    private int grade;
    private int feesPaid;
    private int feesTotal;

    /**
     *
     * @param id - id - id of the student (unique)
     * @param name - name of the student
     * @param grade - grade for the student
     * Fees for every student is 30.000/year
     * Fees paid initially is 0
     */
    public Student(int id, String name, int grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.feesPaid = 0;
        this.feesTotal = 30000;
    }

    public void setGrade(int grade){
        this.grade = grade;
    }

    /**
    * Track the pay fees of a student
     */
    public void payFees(int fees){
        feesPaid = feesPaid + fees;
        School.updateTotalMoneyEarned(feesPaid);
    }

    /**
     * @return the remaining fees of a student
     */
    public int getRemaningFees(){
        return feesTotal - feesPaid;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public int getFeesPaid() {
        return feesPaid;
    }

    public int getFeesTotal() {
        return feesTotal;
    }

    @Override
    public String toString() {
        return "Student's name is: " + name + "\n"+
                "Total fees paid so far is: " + feesPaid;
    }
}
