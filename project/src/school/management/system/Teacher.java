package school.management.system;

/*
* This class is responsible for keeping track of the teacher's id, name, salary
* Salary will be an int, not double
* Teacher id and name are not going to be changed -> no setter method
* */
public class Teacher extends Person {
    
    private int salary;
    private int salaryEarned;

    /**
     * @param id - id of the teacher
     * @param name - name of the teacher
     * @param salary - salary of the teacher
     */
    public Teacher(int id, String name, int salary){
        this.id = id;
        this.name = name;
        this.salary = salary;
        salaryEarned = 0;

    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public int getSalary(){
        return salary;
    }

    /**
     * Set the salary
     */
    public void setSalary(int salary){
        this.salary = salary;
    }

    public void receiveSalary(int salary){
        salaryEarned = salaryEarned + salary;
        School.updateTotalMoneySpent(salary);
    }

    @Override
    public String toString() {
        return "Teachers's name is: " + name + "\n"+
                "Total salary earned so far is: " +  salaryEarned;
    }
}
