package school.management.system;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Teacher paul = new Teacher(1, "Paul", 500);
        Teacher bogdan = new Teacher(2, "Bogdan", 700);
        Teacher claudiu = new Teacher(3, "Claudiu", 600);

        List<Teacher> teachersList = new ArrayList<>();
        teachersList.add(paul);
        teachersList.add(bogdan);
        teachersList.add(claudiu);


        Student mihai = new Student(1, "Mihai", 4);
        Student andrei = new Student(2, "Andrei", 10);
        Student ana = new Student(3, "Ana", 5);

        List<Student> studentsList = new ArrayList<>();
        studentsList.add(mihai);
        studentsList.add(andrei);
        studentsList.add(ana);


        School sincai = new School(teachersList, studentsList);

        Teacher alex = new Teacher(4, "Alex", 1000);

        sincai.addTeacher(alex);

        System.out.println("Sincai first earnings " + sincai.getTotalMoneyEarned() + "\n");

        mihai.payFees(5000);
        System.out.println(mihai + "\n");

        System.out.println("Sincai earned: " + sincai.getTotalMoneyEarned() + "\n");

        System.out.println("----- PAYING SALARIES -----");
        paul.receiveSalary(paul.getSalary());
        System.out.println(sincai.getTotalMoneySpent()+ "\n");

        System.out.println(paul+ "\n");



    }
}
